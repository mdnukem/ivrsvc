package ivrsvc

import (
	"encoding/json"
	"errors"
	"github.com/op/go-logging"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

const (
	idmCred    = "medrec_app:admin"
	idmBase    = "10.36.149.133:8080"
	idmQR      = "IDM/subject/generateQrCode/{idm_id}?targetUri=show"
	idmSubject = "IDM/adt?subject_token={token}"

	medhubBase  = "10.36.149.133:8000"
	userService = "service.user/demographics?token={token}"
	medService  = "records/{subject_id}/medications?token={token}&reviewable=true"

	defaultProtocol = "http"
)

var svcLog = logging.MustGetLogger("userService")
var logFmt = logging.MustStringFormatter("%{color}%{time:15:04:05.000} %{shortfunc}-> %{level:.5s} %{id:03x}%{color:reset} %{message}")
var logBackend = logging.NewLogBackend(os.Stderr, "", 0)
var fmtr = logging.NewBackendFormatter(logBackend, logFmt)

type Patient struct {
	Id        string `json:"id"`
	GivenName string `json:"firstName" binding:"required"`
	Surname   string `json:"lastName" binding:"required"`
	Sex       string `json:"sex"`
	Birthday  string `json:"birthDate"`
}

//PatientDetail queries the idm service for a token corresponding to a particular id number.
//It then calls MedHub and returns the patient's demographic information.
func PatientDetail(idm_id int) (Patient, error) {

	token, err := getPatientToken(idm_id)
	if err != nil {
		return Patient{}, errors.New("Cannot find patient record.")
	}

	qurl := constructPatientQuery(token)
	resp, err := makePatientQuery(qurl, "")
	if err != nil {
		svcLog.Debug(err.Error())
		return Patient{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return Patient{}, errors.New("Cannot find patient record.")
	}

	b, _ := ioutil.ReadAll(resp.Body)
	var newpt Patient
	e := json.Unmarshal(b, &newpt)
	newpt.Id = getUserSubjectId(idm_id)
	return newpt, e
}

//***************** Local Functions *****************

func getPatientToken(idm_id int) (string, error) {
	if idm_id == 360448 {
		return "eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE0MzUxODAzMTg2MTcsInN1YmplY3RJZCI6MzYwNDQ4LCJqdGkiOiJhYjQ1MjY0NC00NDYxLTQ3NGUtOGE2NC1lZGVkMmFiYjQxMmQiLCJpYXQiOjE0MzUxNzg1MTg2MTd9.JkZRw42RCwJYKTRjPqV7ldNfHqeD3HpeTIkbvQQvi_UucJH2Y7450Lr6brzC5yaICChKkRk4wV2YTtp5Wn9-ZQ", nil
	} else {
		return "aninvalidtoken", errors.New("Unable to retrieve patient token for supplied IDM id.")
	}
}

//Stub function to get a user's 'subject id', which links
//them to a medication list.
func getUserSubjectId(idm_id int) string {
	if idm_id == 360448 {
		return "98989898"
		//return "565656"
	} else {
		return "invalidid"
	}
}

func constructPatientQuery(token string) string {
	m := make(map[string]string)
	m["{token}"] = token

	return constructURL(defaultProtocol, idmBase, idmSubject, "medrec_app:admin", m)
}

func makePatientQuery(qurl string, qb string) (*http.Response, error) {
	return http.Post(qurl, qb, nil)
}

func isJSONResponse(r io.Reader) bool {
	type Message struct {
		Name, Text string
	}

	dec := json.NewDecoder(r)
	for {
		var m Message
		err := dec.Decode(&m)
		if err == io.EOF {
			return true
		} else if err != nil {
			return false
		}
	}
}

func constructURL(proto string, base string, location string, creds string, replace map[string]string) string {
	var newurl string

	if creds != "" {
		newurl = proto + "://" + creds + "@" + base + "/" + location
	} else {
		newurl = proto + "://" + base + "/" + location
	}

	for key, value := range replace {
		newurl = strings.Replace(newurl, key, value, -1)
	}
	return newurl
}
