# IVR Service
The IVR service provides a number of services to the API to allow MedHub and the i2me2 IVR application to communicate.  

## Med Service
Med service is the main translation layer between MedHub and a format the IVR can understand.  Struct are defined with tags in the Med Service that allow JSON responses from MedHub to be automatically unmarshaled.  After the necessary field are unmarshaled, transformed, and massaged into something that works with the IVR, the data is sent on to the IVR.

Once the IVR has finished getting responses from the user, it sends back an XML file, which has the general form of the file at ivr_api/res/sample_response.xml within the main IVR API project.

The responses sent back by the IVR are currently in an odd XML format due to some oddities/outdatedness in the IVR toolchain.  The Med Service API umarshals the responses according to the tags on the structs at the top of medsvc.go.  Note that Medication objects are unmarshaled according to the "json" struct tags, but the Golang's standard JSON unmarshaling is not used due to the IVR's response being XML.

This means that only strings are currently supported in the Medication struct if you want them to be populated correctly by IVR responses (this can be fixed relatively easily).  Also, the "required" tags in the Medication struct will have no impact when unmarshaling the IVR responses (i.e. no error will occur if an IVR response does not have a field marked as "required.").  The "required" tags **will** cause an error if a medication in the initial response *from MedHub* doesn't have one of them, though. 

##Tests
To run the ivrsvc tests, simply run "go test" or "go test -v" from the command line in the ivrsvc/ directory.      