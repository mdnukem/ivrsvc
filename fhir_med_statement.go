package ivrsvc

import (
	"bytes"
	"errors"
	"github.com/op/go-logging"
	"io"
	"net/http"
	"os"
	"reflect"
	"strconv"
	"text/template"
	"time"
)

//************************* Constants ********************************
//********************************************************************

const (
	RESOURCE_DIR           = "res/"
	FHIR_MEDSTATE_TEMPLATE = "FHIR-medicationstatement-0.5.0.5149-contained.json"
	TEST_TEMPLATE          = "test_template.json"
	VIT_TEMPLATE           = "vitamin_template.json"

	fhirUpdateEndpoint = "records/{subject_id}/medications?token={token}"
)

//************************* Exported Functions ***********************
//********************************************************************

func SendMedicationStatement(mu *MedicationUpdate, pt *Patient, idm_id int) error {
	var bytebuf bytes.Buffer
	m := make(map[string]string)

	tok, err := getPatientToken(idm_id)
	if err != nil {
		return err
	}

	m["{token}"] = tok
	m["{subject_id}"] = strconv.Itoa(idm_id)

	err = createMedStatement(mu, pt, &bytebuf)
	if err != nil {
		return errors.New("Unable to create medication statement.")
	}

	qurl := constructURL("http", medhubBase, fhirUpdateEndpoint, "", m)
	_, err = sendMedicationStatementUpdate(qurl, &bytebuf)

	return err
}

func SendVitaminStatment(vu *VitaminUpdate, pt *Patient, idm_id int) error {
	var bytebuf bytes.Buffer
	m := make(map[string]string)

	tok, err := getPatientToken(idm_id)
	if err != nil {
		return err
	}
	m["{token}"] = tok
	m["{subject_id}"] = strconv.Itoa(idm_id)

	err = createVitStatement(vu, pt, &bytebuf)
	if err != nil {
		return errors.New("Unable to create medication statement for vitamin.")
	}

	qurl := constructURL("http", medhubBase, fhirUpdateEndpoint, "", m)
	_, err = sendMedicationStatementUpdate(qurl, &bytebuf)

	return err
}

//************************* Local Functions **************************
//********************************************************************

func createMedicationStatement(data interface{}, destbuf *bytes.Buffer, templateFile string, fmap template.FuncMap) error {
	var theTemplate *template.Template

	if reflect.TypeOf(data).Kind() != reflect.Struct {
		return errors.New("Data passed in to createMedicationStatement must be a struct.")
	}

	theTemplate = template.New(templateFile).Funcs(fmap)
	_, err := theTemplate.ParseFiles(RESOURCE_DIR + templateFile)
	if err != nil {
		return err
	}

	err = theTemplate.ExecuteTemplate(destbuf, templateFile, data)
	if err != nil {
		return err
	}

	logToFile(destbuf.String())

	return err
}

func createMedStatement(mu *MedicationUpdate, pt *Patient, buf *bytes.Buffer) error {
	fm := template.FuncMap{
		"isEmptyString":     isEmptyString,
		"takesRegularly":    takesRegularly,
		"takesAsNeeded":     takesAsNeeded,
		"getFrequencyUnits": getFrequencyUnits,
		"getDosageQuantity": getDosageQuantity,
		"getCurrentTime":    getCurrentTime,
	}

	//An anonymous struct to hold all of the data the template will need
	tdata := struct {
		Med    *Medication
		MedUse *MedicationUsage
		Pt     *Patient
	}{
		&mu.Medication,
		&mu.Responses,
		pt,
	}

	return createMedicationStatement(tdata, buf, TEST_TEMPLATE, fm)
}
func createVitStatement(vu *VitaminUpdate, pt *Patient, buf *bytes.Buffer) error {
	fm := template.FuncMap{
		"isEmptyString":  isEmptyString,
		"getCurrentTime": getCurrentTime,
		"takesRegularly": takesRegularly,
	}

	tdata := struct {
		Vit *Vitamin
		Use *MedicationUsage
		Pt  *Patient
	}{
		&vu.Vitamin,
		&vu.MedicationUsage,
		pt,
	}
	return createMedicationStatement(tdata, buf, VIT_TEMPLATE, fm)
}

func sendMedicationStatementUpdate(url string, body io.Reader) (*http.Response, error) {
	return http.Post(url, "application/json", body)
}

func logToFile(message string) {
	var f, err = os.OpenFile("fhirLog.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		svcLog.Error("Error when opening FHIR Log file: %v", err.Error())
		return
	}
	defer f.Close()

	var fhirLog = logging.MustGetLogger("fhirService")
	var fLogFmt = logging.MustStringFormatter("%{color}%{time:15:04:05.000} %{shortfunc}-> %{level:.5s} %{id:03x}%{color:reset} %{message}")
	var fLogBackend = logging.NewLogBackend(f, "", 0)
	var fFmtr = logging.NewBackendFormatter(fLogBackend, fLogFmt)
	logging.SetBackend(fFmtr)
	fhirLog.Info("\n%v\n\n", message)
}

//************************* Template Functions ***********************
//********************************************************************

func isEmptyString(arg string) bool {
	if arg == "" {
		return true
	} else {
		return false
	}
}

func takesRegularly(response *MedicationUsage) bool {
	if takesDaily(response) || takesWeekly(response) || takesMonthly(response) {
		return true
	} else {
		return false
	}
}

func takesDaily(response *MedicationUsage) bool {
	if response.FrequencyDenominator == "day" {
		return true
	} else {
		return false
	}
}

func takesWeekly(response *MedicationUsage) bool {
	if response.FrequencyDenominator == "week" {
		return true
	} else {
		return false
	}
}

func takesMonthly(response *MedicationUsage) bool {
	if response.FrequencyDenominator == "month" {
		return true
	} else {
		return false
	}
}

func takesAsNeeded(response *MedicationUsage) bool {
	if response.FrequencyDenominator == "as_needed" {
		return true
	} else {
		return false
	}
}

func getFrequencyUnits(response *MedicationUsage) string {
	switch response.FrequencyDenominator {
	case "day":
		return "d"
	case "week":
		return "wk"
	case "month":
		return "mo"
	default:
		return "<INVALID>"
	}
}

func getDosageQuantity(response *MedicationUsage) int {
	return response.DosesPerIntake
}

func getCurrentTime() string {
	return time.Now().Format(time.RFC3339)
}
