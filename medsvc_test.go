package ivrsvc

//Run these tests with the command "go test" or "go test -v" from the ivrsvc/ directory.

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"io/ioutil"
	"testing"
)

var (
	RxtDb *sql.DB
)

func IVRUpdateSetup(t *testing.T, updateFilename string, dbFilename string) ([]byte, *sql.DB, error) {
	bb, err := ioutil.ReadFile(updateFilename)
	if err != nil {
		return []byte{}, nil, err
	}

	var dberr error
	RxtDb, dberr = sql.Open("sqlite3", dbFilename)
	if dberr != nil {
		return []byte{}, nil, dberr
	}

	return bb, RxtDb, nil
}

func TestIVRMedicationUpdate(t *testing.T) {

	bb, RxtDb, err := IVRUpdateSetup(t, "test_res/sample_response.xml", "test_res/rxterms.db")
	if err != nil {
		t.Fatalf("Failed to setup test conditions.  Error: '%v'", err.Error())
	}
	defer RxtDb.Close()

	var updates []MedicationUpdate
	err = UnmarshalIVRMedUpdate(bb, &updates, RxtDb)
	if err != nil {
		t.Fatalf("Received this unexpected error when unmarshalling medication updates: '%v'", err.Error())
	}
	//Check for the expected number of medication updates.
	if len(updates) != 5 {
		t.Fatalf("Failed to unmarshal all of the IVR responses.")
	} else {
		t.Logf("Unmarshalled %v medication updates from the test IVR response.", len(updates))
	}

	//Check that all of the medication updates are associated with an RxCUI
	for i, _ := range updates {
		if updates[i].Medication.RxCUI == "" {
			t.Error("An unmarshalled MedicationUpdate unexpectedly had an empty RxCUI")
		} else {
			t.Logf("Found a RxCUI in an unmarshalled MedicationUpdate: %v", updates[i].Medication.RxCUI)
		}
	}

	if updates[1].Medication.AlternativeName != "Lanoxin" {
		t.Errorf("Failed to properly unmarshal the medication's alt name: %v", updates[1])
	}
	if updates[1].Medication.Brand != "lanoxin" {
		t.Errorf("Failed to properly unmarshal medication's brand name: %v", updates[1])
	}
	if updates[1].Medication.Form != "tablet" {
		t.Errorf("Failed to properly unmarshal medication's form: %v", updates[1])
	}
	if updates[1].Medication.Strength != "0.125 mg" {
		t.Errorf("Failed to properly unmarshal medication's strength: %v", updates[1])
	}
	if updates[1].Medication.NDC != "00173024275" {
		t.Errorf("Failed to properly unmarshal medication's NDC: %v", updates[1])
	}
	if updates[1].Medication.Ingredient != "digoxin" {
		t.Errorf("Failed to properly unmarshal medication's ingredient: %v", updates[1])
	}
	if updates[1].Responses.CorrectForm != true || updates[1].Responses.CorrectStrength != true {
		t.Errorf("Failed to properly unmarshal the form and strength responses for the medication: %v", updates[1])
	}
	if updates[1].Responses.CurrentlyTakes != true {
		t.Errorf("Failed to properly unmarshal the currently takes response for the medication: %v", updates[1])
	}
	if updates[1].Responses.DosesPerIntake != 2 {
		t.Errorf("Failed to properly unmarshal the doses per intake response for the medication: %v", updates[1])
	}
	if updates[1].Responses.Frequency != 1 {
		t.Errorf("Failed to properly unmarshal the frequency response for the medication: %v", updates[1])
	}
	if updates[1].Responses.FrequencyDenominator != "day" {
		t.Errorf("Failed to properly unmarshal the frequency denominator for the medication: %v", updates[1])
	}
}

func TestIVRVitaminUpdate(t *testing.T) {
	bb, RxtDb, err := IVRUpdateSetup(t, "test_res/sample_vit_response.xml", "test_res/rxterms.db")
	if err != nil {
		t.Fatalf("Failed to setup test conditions.  Error: '%v'", err.Error())
	}
	defer RxtDb.Close()

	var updates []VitaminUpdate
	err = UnmarshalIVRVitaminUpdate(bb, &updates)
	if err != nil {
		t.Fatalf("Received this unexpected error when unmarshalling vitamin updates: '%v'", err.Error())
	}
	//Check for the expected number of vitamin updates
	if len(updates) != 13 {
		t.Fatalf("Failed to unmarshal all of the IVR vitamin updates.  Only found %v, expected %v.", len(updates), 13)
	} else {
		t.Logf("Unmarshalled %v vitamin updates from the tested IVR response.", len(updates))
	}

	//Check that all of the vitamin responses have RxCUIs
	for i, _ := range updates {
		if updates[i].Vitamin.RxCUI == "" {
			t.Errorf("An unmarshalled VitaminUpdate unexpectedly had an empty RxCUI.")
		} else {
			t.Logf("Found a RxCUI in an unmarshalled VitaminUpdate: %v", updates[i].Vitamin.RxCUI)
		}

		//Check that the right vitamin update has CurrentlyTakes set to true
		if updates[i].Vitamin.RxCUI == "4511" {
			if updates[i].CurrentlyTakes != true {
				t.Errorf("Expected Vitamin with RxCUI:'%v' to have CurrentlyTakes == true.", updates[i].Vitamin.RxCUI)
			}
		}
	}

}
