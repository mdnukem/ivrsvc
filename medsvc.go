package ivrsvc

import (
	"database/sql"
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"io/ioutil"
	"net"
	"net/http"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"time"
)

//************************* Exported Structs *************************
//********************************************************************

//Will be used
/*
type UMLSEntry struct {
	RxCUI string `json:"rxcui" binding:"required"`
}*/

//A struct containing all the information required to uniquely identify
//a single over-the-counter vitamin
type Vitamin struct {
	RxCUI      string `json:"rxcui" binding:"required"`
	SpokenName string `json:"en-US"`
	Name       string `json:"name"`
}

type VitaminUpdate struct {
	Vitamin
	MedicationUsage
}

//A struct containing all information required to uniquely identify
//a single medication.
type Medication struct {
	MedLabel string `json:"label"`
	RxCUI    string `json:"rxcui" binding:"required"`
	Id       string `json:"_id" binding:"required"`
	Name     string `json:"name"`
	NDC      string `json:"ndc" binding:"required"`

	AlternativeName string `json:"alt_name"`
	Ingredient      string `json:"active_ingredient"`
	Brand           string `json:"brand_name"`
	Form            string `json:"form"`
	Strength        string `json:"strength"`

	//UMLSEntry
}

//Represents a response from the MedHub service, containing all medications listed
//for an individual
type MedicationResponse struct {
	Data      []Medication `json:"data"`
	SubjectId string       `json:"record_id"`
	Count     int          `json:"count"`
}

//A struct containing information about how a person is using a medication
type MedicationUsage struct {
	CurrentlyTakes       bool   `medresponse:"currently_takes"`
	CorrectForm          bool   `medresponse:"correct_form"`
	CorrectStrength      bool   `medresponse:"correct_strength"`
	FrequencyDenominator string `medresponse:"freq_denom"`
	Frequency            int    `medresponse:"freq"`
	DosesPerIntake       int    `medresponse:"doses_per_intake"`
}

//An update to a single medication
type MedicationUpdate struct {
	Medication Medication
	Responses  MedicationUsage
}

//************************* Local Structs ****************************
//********************************************************************

//A struct representing an update document, as sent by the IVR or another source
type xmlUpdateDoc struct {
	XMLName xml.Name `xml:"root"`
	Rows    []xmlRow `xml:"row"`
}
type xmlRow struct {
	Cols []xmlCol `xml:"col"`
}
type xmlCol struct {
	Key   string `xml:"name,attr"`
	Value string `xml:",chardata"`
}

//************************* Constants ********************************
//********************************************************************

type FREQDENOM int32

const (
	DAY       = iota
	WEEK      = iota
	MONTH     = iota
	AS_NEEDED = iota
)

//********************** Interface Definitions ***********************
//********************************************************************

type IVRResponser interface {
	ReadRow(r *xmlRow) error
	ReadCol(c *xmlCol) error
}

//************************* Exported Functions ***********************
//********************************************************************

//PendingMedicationsForPatient retrieves an array of all reviewable
//medications we have for a given patient.
func PendingMedicationsForPatient(idm_id int, db *sql.DB) (MedicationResponse, error) {
	token, err := getPatientToken(idm_id)
	mq := constructPtMedsQuery(token, idm_id)

	resp, err := makeMedicationQuery(mq)
	if err != nil || resp.StatusCode != http.StatusOK {
		return MedicationResponse{}, errors.New("Unable to retrieve medication list.")
	}
	defer resp.Body.Close()

	var medresp MedicationResponse
	b, _ := ioutil.ReadAll(resp.Body)
	e := json.Unmarshal(b, &medresp)

	parseLabels(&medresp)
	removeRetiredRxCUIs(&medresp, db)

	for i, _ := range medresp.Data {
		populateMedicationFromRxCUI(&medresp.Data[i], db)
	}

	medresp.Count = len(medresp.Data)

	return medresp, e
}

//Given an IVR update of the form shown in test_res/sample_response.xml, UnmarshalIVRUpdate
//will unmarshal it into a MedicationUpdate slice.  Will return an error if any of the rows
//in the update cannot be unmarshalled.
func UnmarshalIVRMedUpdate(data []byte, up *[]MedicationUpdate, medInfoDB *sql.DB) error {
	var updateDoc xmlUpdateDoc

	err := xml.Unmarshal(data, &updateDoc)
	if err != nil {
		return err
	}

	for ri, _ := range updateDoc.Rows {
		var update MedicationUpdate
		err = update.ReadRow(&updateDoc.Rows[ri])
		if err != nil {
			return errors.New(
				fmt.Sprintf("Could not created medication update from row %v of the response.", ri))
			break
		}
		*up = append(*up, update)
	}

	return nil
}

//Retrieves a list of all types of vitamin supplements a person could be taking
func GetVitaminList() ([]Vitamin, error) {
	return retrievePossibleVitamins()
}

//Given an IVR update of the form shown in test_res/sample_vit_response.xml,
//UnmarshalIVRVitaminUpdate will unmarshal it into a VitaminUpdate slice.  Will return an error if
//any of the rows in the update cannot be unmarshalled.
func UnmarshalIVRVitaminUpdate(data []byte, up *[]VitaminUpdate) error {
	var updateDoc xmlUpdateDoc

	err := xml.Unmarshal(data, &updateDoc)
	if err != nil {
		return err
	}

	for ri, _ := range updateDoc.Rows {
		var update VitaminUpdate
		err = update.ReadRow(&updateDoc.Rows[ri])
		if err != nil {
			return errors.New(
				fmt.Sprintf("Could not create vitamin update from row %v of the response.", ri))
			break
		}
		*up = append(*up, update)
	}

	return nil
}

//************************* Interface Implementations ****************
//********************************************************************

func (MU *MedicationUpdate) String() string {
	var toPrint string = ""
	toPrint += MU.Medication.String()
	toPrint += MU.Responses.String()
	toPrint += "\n"

	return toPrint
}

func (Med *Medication) String() string {
	var toPrint string = ""
	toPrint += "RxCUI: " + Med.RxCUI + "\n\t"
	toPrint += "Brand Name: " + Med.Brand + "\n\t"
	toPrint += "Active Ingredient(s): " + Med.Ingredient + "\n\t"
	toPrint += "Form: " + Med.Form + "\n\t"
	toPrint += "Strength: " + Med.Strength + "\n\t"
	toPrint += "Label: " + Med.MedLabel + "\n"

	return toPrint
}

func (Resp *MedicationUsage) String() string {
	var toPrint string = ""
	toPrint += "The respondent "
	if Resp.CurrentlyTakes == true {
		toPrint += "currently takes "
	} else {
		toPrint += "currently does not take "
	}
	toPrint += "the medication.\n"

	if Resp.CurrentlyTakes == true {
		if Resp.CorrectStrength == true {
			toPrint += "The strength is correct.\n"
		} else {
			toPrint += "The strength is marked incorrect.\n"
		}

		if Resp.CorrectForm == true {
			toPrint += "The form is correct.\n"
		} else {
			toPrint += "The form is marked incorrect.\n"
		}

		if Resp.FrequencyDenominator != "as needed" {
			toPrint += "The respondent takes " +
				strconv.Itoa(Resp.Frequency) + " per " + Resp.FrequencyDenominator
			toPrint += "\n"
		} else {
			toPrint += "The respondent takes the drug " + Resp.FrequencyDenominator + "\n"
		}

		toPrint += "The respondent takes " +
			strconv.Itoa(Resp.DosesPerIntake) + " doses each time they take the medication.\n"
	}

	return toPrint
}

func (row *xmlRow) String() string {
	var toPrint string = ""
	toPrint += fmt.Sprintf("Row has %v columns:\n\t", len(row.Cols))
	for i, _ := range row.Cols {
		toPrint += row.Cols[i].String()
	}

	return toPrint
}

func (col *xmlCol) String() string {
	var toPrint string = ""
	toPrint += fmt.Sprintf("Col Key: '%v', Value: '%v'\n\t", col.Key, col.Value)
	return toPrint
}

//Implementation of IVRResponser for *MedicationUpdate
func (r *MedicationUpdate) ReadRow(row *xmlRow) error {
	e := r.Medication.ReadRow(row)
	if e != nil {
		return e
	}

	e = r.Responses.ReadRow(row)
	if e != nil {
		return e
	}

	return nil
}

//Implementation of IVRResponser for *MedicationUpdate
func (r *MedicationUpdate) ReadCol(col *xmlCol) error {
	return nil
}

//Implementation of IVRResponser for *VitaminUpdate
func (r *VitaminUpdate) ReadRow(row *xmlRow) error {
	e := r.Vitamin.ReadRow(row)
	if e != nil {
		return e
	}

	e = r.MedicationUsage.ReadRow(row)
	if e != nil {
		return e
	}
	return nil
}

//Implementation of IVRResponser for *VitaminUpdate
func (r *VitaminUpdate) ReadCol(col *xmlCol) error {
	return genericReadCol(r, col, "json")
}

//Implementation of IVRResponser for *Vitamin
func (r *Vitamin) ReadRow(row *xmlRow) error {
	return genericReadRow(r, row)
}

//Implementation of IVRResponser for *Vitamin
func (r *Vitamin) ReadCol(col *xmlCol) error {
	return genericReadCol(r, col, "json")
}

//Implementation of IVRResponser for *Medication
func (r *Medication) ReadRow(row *xmlRow) error {
	return genericReadRow(r, row)
}

//Implementation of IVRResponser for *Medication
func (r *Medication) ReadCol(col *xmlCol) error {
	return genericReadCol(r, col, "json")
}

//Implementation of IVRResponser for *MedicationUsage
func (r *MedicationUsage) ReadRow(row *xmlRow) error {
	return genericReadRow(r, row)
}

//Implementation of IVRResponser for *MedicationUsage
func (r *MedicationUsage) ReadCol(col *xmlCol) error {
	return genericReadCol(r, col, "medresponse")
}

//************************* Local Functions **************************
//********************************************************************

func genericReadCol(dest interface{}, col *xmlCol, tagval string) error {
	pt := reflect.TypeOf(dest)
	if pt.Kind() != reflect.Ptr {
		return errors.New("Tried to call genericReadCol with a non-pointer destination.")
	}
	if pt.Elem().Kind() != reflect.Struct {
		return errors.New("Destination pointer when reading column dereferenced to non-struct Kind.")
	}

	for j := 0; j < pt.Elem().NumField(); j++ {
		v := reflect.ValueOf(dest).Elem().Field(j)
		fieldTag := pt.Elem().Field(j).Tag.Get(tagval)
		if fieldTag == col.Key && col.Value != "" {
			e := setFieldValue(v, col.Value)
			if e != nil {
				return e
			}
		}
	}

	return nil
}

func genericReadRow(dest IVRResponser, row *xmlRow) error {
	for i, _ := range row.Cols {
		e := dest.ReadCol(&row.Cols[i])
		if e != nil {
			return e
		}
	}
	return nil
}

func setFieldValue(dest reflect.Value, v interface{}) error {
	if dest.IsValid() {
		if dest.CanSet() {
			switch {
			case dest.Kind() == reflect.Bool:
				switch {
				case reflect.TypeOf(v).Kind() == reflect.String:
					b, e := strconv.ParseBool(v.(string))
					if e != nil {
						return e
					} else {
						dest.SetBool(b)
					}
				default:
					return errors.New(fmt.Sprintf("Cannot convert Type '%v' to Kind '%v'", reflect.TypeOf(v), dest.Kind()))
				}
			case dest.Kind() == reflect.Int:
				switch {
				case reflect.TypeOf(v).Kind() == reflect.String:
					i, e := strconv.ParseInt(v.(string), 10, 32)
					if e != nil {
						return e
					} else {
						dest.SetInt(i)
					}
				default:
					return errors.New(fmt.Sprintf("Cannot convert Type '%v' to Kind '%v'", reflect.TypeOf(v), dest.Kind()))
				}
			case dest.Kind() == reflect.Int32:
				switch {
				case reflect.TypeOf(v).Kind() == reflect.String:
					i, e := strconv.ParseInt(v.(string), 10, 32)
					if e != nil {
						return e
					} else {
						dest.SetInt(i)
					}
				default:
					return errors.New(fmt.Sprintf("Cannot convert Type '%v' to Kind '%v'", reflect.TypeOf(v), dest.Kind()))
				}
			case dest.Kind() == reflect.String:
				switch {
				case reflect.TypeOf(v).Kind() == reflect.String:
					dest.SetString(v.(string))
				default:
					return errors.New(fmt.Sprintf("Cannot convert Type '%v' to Kind '%v'", reflect.TypeOf(v), dest.Kind()))
				}
			default:
				return errors.New(fmt.Sprintf("Cannot convert to a destination Kind = '%v'", dest.Kind()))
			}
		} else {
			return errors.New(fmt.Sprintf("The destination Kind %v was not settable.", dest.Kind()))
		}
	} else {
		return errors.New(fmt.Sprintf("The destination Kind %v did not actually represent a value.", dest.Kind()))
	}

	return nil
}

func makeMedicationQuery(url string) (*http.Response, error) {
	transport := http.Transport{
		Dial: dialTimeout,
	}

	client := http.Client{
		Transport: &transport,
	}

	return client.Get(url)
}

func dialTimeout(network, addr string) (net.Conn, error) {
	timeout := time.Duration(20 * time.Second)
	return net.DialTimeout(network, addr, timeout)
}

func newMedicationFromRxCUI(rxcui string, medInfoDB *sql.DB) *Medication {
	m := Medication{RxCUI: rxcui}
	populateMedicationFromRxCUI(&m, medInfoDB)
	return &m
}

func populateMedicationFromRxCUI(m *Medication, medInfoDB *sql.DB) {
	getGenericName(m, medInfoDB)
	getBrandName(m, medInfoDB)
	getForm(m, medInfoDB)
	getStrength(m, medInfoDB)
}

func constructPtMedsQuery(token string, idm_id int) string {
	m := make(map[string]string)
	m["{token}"] = token
	m["{subject_id}"] = getUserSubjectId(idm_id)

	return constructURL(defaultProtocol, medhubBase, medService, "", m)
}

func parseLabels(response *MedicationResponse) {
	//Removes the RxNorm TTY at the end of the string (e.g. [SBD])
	re := regexp.MustCompile(`\s*\[S[BC]D\]\s*$`)

	//Handles forward slashes for concentrations (e.g. mg/ml goes to mg per ml)
	perReg := regexp.MustCompile(`(\w{1,3})/(\w{1,3})`)

	//Extracts an alternative name from the label string
	lreg := regexp.MustCompile(`\s*\[(\w+)\]$`)

	for x := 0; x < len(response.Data); x++ {
		response.Data[x].MedLabel = re.ReplaceAllString(response.Data[x].MedLabel, "")
		response.Data[x].MedLabel = perReg.ReplaceAllString(response.Data[x].MedLabel, "${1} per ${2}")
		response.Data[x].AlternativeName = lreg.FindStringSubmatch(response.Data[x].MedLabel)[1]
		response.Data[x].MedLabel = lreg.ReplaceAllString(response.Data[x].MedLabel, "")
	}
}

func getGenericName(med *Medication, db *sql.DB) error {
	rows, err := db.Query("select INGREDIENT from INGREDIENTS where RXCUI=?", med.RxCUI)
	if err != nil {
		return err
	}
	defer rows.Close()

	var ing string
	for rows.Next() {
		err = rows.Scan(&ing)
		if err != nil {
			return err
		}

		if med.Ingredient != "" {
			med.Ingredient += " and " + strings.ToLower(ing)
		} else {
			med.Ingredient = strings.ToLower(ing)
		}
	}
	if err := rows.Err(); err != nil {
		return err
	}

	return nil
}

func getBrandName(med *Medication, db *sql.DB) error {
	var bn string

	err := db.QueryRow("select BRAND_NAME from RXTERMS where RXCUI=?", med.RxCUI).Scan(&bn)
	switch {
	case err == sql.ErrNoRows:
	case err != nil:
		return err
	default:
		med.Brand = strings.ToLower(bn)
	}
	if err != nil {
		return err
	}

	return nil
}

//getForm returns the form that the medication is typically presented in (e.g. tablet, capsule, inhaler, etc)
func getForm(med *Medication, db *sql.DB) error {
	rows, err := db.Query("select NEW_DOSE_FORM from RXTERMS where RXCUI=?", med.RxCUI)
	if err != nil {
		return err
	}
	defer rows.Close()

	var oldform string
	for rows.Next() {
		err = rows.Scan(&oldform)
		if err != nil {
			return err
		}

		var newform string
		err = db.QueryRow(
			"select MAPPED_FORM from FORM_MAPPINGS where ORIGINAL_FORM=?", oldform).Scan(&newform)
		switch {
		//Note: The ErrNoRows is meant to be a no-op (fallthrough is not default in Go).
		case err == sql.ErrNoRows:
		case err != nil:
			svcLog.Error(
				"\tGot error: '%v' while looking for a mapped form for RxCUI='%v'",
				err.Error(),
				med.RxCUI)
			return err
		default:
			med.Form = strings.ToLower(newform)
		}
	}
	if err = rows.Err(); err != nil {
		return err
	}

	return nil
}

func getStrength(med *Medication, db *sql.DB) error {
	rows, err := db.Query("select STRENGTH from RXTERMS where RXCUI=?", med.RxCUI)
	if err != nil {
		svcLog.Error("%v", err.Error())
		return err
	}
	defer rows.Close()

	var strength string
	for rows.Next() {
		err = rows.Scan(&strength)
		if err != nil {
			svcLog.Error("%v", err.Error())
			return err
		}
		med.Strength = strength
	}
	if err = rows.Err(); err != nil {
		svcLog.Error("%v", err.Error())
		return err
	}
	return nil
}

//Returns true if the RxCUI is retired, false if the RxCUI is still active.
//Currently using a bit of a surrogate by saying if something is in the RxTerms DB, it's
//active, and if it's missing, it's retired.
func checkIfRxCUIRetired(med *Medication, db *sql.DB) (bool, error) {
	var retval bool = false
	var temp string
	err := db.QueryRow("select FULL_NAME from RXTERMS where RXCUI=?", med.RxCUI).Scan(&temp)
	switch {
	case err == sql.ErrNoRows:
		retval = true
	case err != nil && err != sql.ErrNoRows:
		svcLog.Error("Got error: '%v' while looking for RxCUI='%v'", err.Error(), med.RxCUI)
		return false, err

	default:
		retval = false
	}

	return retval, nil
}

func removeRetiredRxCUIs(medresp *MedicationResponse, db *sql.DB) error {

	for i := 0; i < len(medresp.Data); i++ {
		isret, err := checkIfRxCUIRetired(&medresp.Data[i], db)

		if isret == true && err == nil {
			//All this to remove an element from the slice o_O
			copy(medresp.Data[i:], medresp.Data[i+1:])
			medresp.Data[len(medresp.Data)-1] = Medication{}
			medresp.Data = medresp.Data[:len(medresp.Data)-1]
		} else if err != nil {
			return err
		}
	}

	return nil
}

func retrievePossibleVitamins() ([]Vitamin, error) {
	//For now, we will have a simple hard-coded list of vitamins
	//Map key=rxcui value=spoken name
	vrx := make(map[string]string)
	vrx["11246"] = "Vitamin A"
	vrx["10454"] = "Vitamin B 1"
	vrx["9346"] = "Vitamin B 2"
	vrx["7393"] = "Vitamin B 3"
	vrx["7891"] = "Vitamin B 5"
	vrx["42954"] = "Vitamin B 6"
	vrx["1588"] = "Vitamin B 7"   //Biotin
	vrx["4511"] = "Vitamin B 9"   //Folic Acid
	vrx["11248"] = "Vitamin B 12" //Cyanocobalamin
	vrx["1151"] = "Vitamin C"     //Ascorbic Acid
	vrx["11253"] = "Vitamin D"    //Cholcalciferol (D2) Ergocalciferol (D3)
	vrx["11256"] = "Vitamin E"    //Tocopherols
	vrx["11258"] = "Vitamin K"    //phylloquinone

	varr := make([]Vitamin, 0)

	for k, v := range vrx {
		newvit := Vitamin{
			RxCUI:      k,
			SpokenName: v,
			Name:       v,
		}
		varr = append(varr, newvit)
	}

	return varr, nil
}
